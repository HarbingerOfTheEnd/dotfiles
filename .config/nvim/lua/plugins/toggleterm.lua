local ok, toggleterm = pcall(require, "toggleterm")
if not ok then return end

local shell

if vim.fn.has("win32") then
    shell = "pwsh.exe"
else
    shell = "zsh"
end

toggleterm.setup {
    open_mapping = [[<leader>t]],
    shell = shell,
    insert_mapping = false,
}

local function set_terminal_keymaps()
    local opts = { buffer = 0 }
    vim.keymap.set("t", "<esc>", [[<C-\><C-n>]], opts)
end

vim.api.nvim_create_autocmd("TermOpen", {
    pattern = "term://*",
    callback = set_terminal_keymaps,
})
